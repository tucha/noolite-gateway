package homekit

import (
	"log"
	"strconv"
	"sync"
	"time"

	"github.com/brutella/hc"
	"github.com/brutella/hc/accessory"
)

//HomeKitSwitch is the generic switch with feedback
type HomeKitSwitch interface {
	State() bool
	Callback(clb func())
	CommandState(st bool)
	UniqueID() string
	Name() string
}
type hkSw struct {
	sw        HomeKitSwitch
	stopFunc  func()
	startFunc func()
}

//HomeKit exposes HomeKitSwitch switches to HomeKit infrastructure
type HomeKit struct {
	switches    map[string]hkSw
	logger      *log.Logger
	pin         string
	ipPortStart int
	dbFolder    string
	mtx         *sync.Mutex
}

func New(logger *log.Logger, pin string, ipPortStart int, dbFolder string) *HomeKit {
	th := &HomeKit{}
	th.switches = make(map[string]hkSw)
	th.logger = logger
	th.pin = pin
	th.ipPortStart = ipPortStart
	th.dbFolder = dbFolder
	th.mtx = &sync.Mutex{}
	return th
}

func (th *HomeKit) Stop() {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	for _, v := range th.switches {
		v.stopFunc()
	}
}
func (th *HomeKit) Remove(br HomeKitSwitch) {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	if d, ok := th.switches[br.UniqueID()]; ok {
		d.stopFunc()
		delete(th.switches, br.UniqueID())
	}
}

//Add registers switch. Call before Run
func (th *HomeKit) Add(br HomeKitSwitch) {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	sw := accessory.NewSwitch(accessory.Info{
		Name:         br.Name(),
		SerialNumber: br.UniqueID(),
	})

	sw.Switch.On.OnValueRemoteUpdate(func(thisBr HomeKitSwitch) func(bool) {
		return func(on bool) {
			if on == true {
				thisBr.CommandState(true)
			} else {
				thisBr.CommandState(false)
			}
		}
	}(br))

	config := hc.Config{Pin: th.pin, Port: strconv.Itoa(th.ipPortStart + len(th.switches)), StoragePath: th.dbFolder + br.UniqueID()}
	var err error
	transport, err := hc.NewIPTransport(config, sw.Accessory)
	if err != nil {
		th.logger.Print(err)
		return
	}

	br.Callback(func(thisSw *accessory.Switch, thisBr HomeKitSwitch) func() {
		return func() {
			st := thisBr.State()
			if st != thisSw.Switch.On.GetValue() {
				thisSw.Switch.On.SetValue(st)
			}
		}
	}(sw, br))
	th.switches[br.UniqueID()] = hkSw{
		sw: br,
		stopFunc: func() {
			<-transport.Stop()
		},
		startFunc: func() {
			go transport.Start()
			time.AfterFunc(time.Second*2, func() {
				firstSt := br.State()
				if firstSt != sw.Switch.On.GetValue() {
					sw.Switch.On.SetValue(firstSt)
				}
			})
		},
	}

}

//Run starts service
func (th *HomeKit) Run() {
	for _, v := range th.switches {
		v.startFunc()
	}
}
