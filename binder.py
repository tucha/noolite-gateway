import requests
import time
# host = "http://192.168.1.179:80"
host = "http://test-noolite-hub.local"

print("get devices")
r = requests.get(host + "/devices")
print(r.json())
print()


print("binding")
r = requests.post(host + "/bind_device", json={"name": "ToiletLamp"})
print(r.json())
print()

print("get devices")
r = requests.get(host + "/devices")
print(r.json())
print()
