package control

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"

	"bitbucket.org/tucha/noolite-gateway/rpiwifi"
)

type GenericDevice interface {
	State() bool
	CommandState(st bool)

	Value() float32
	CommandValue(v float32)

	UniqueID() string
	Name() string
	Readable() bool
	Writable() bool
	IsSmooth() bool
	Unit() string
	ValueMin() float32
	ValueMax() float32

	Description() string

	Callback(clb func())

	ActivateBinding()
}

func (th *control) Remove(sw GenericDevice) {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	delete(th.devMap, sw.UniqueID())
}

func dumpDev(sw GenericDevice) DevState {
	return DevState{
		ID:       sw.UniqueID(),
		Name:     sw.Name(),
		State:    sw.State(),
		Value:    sw.Value(),
		Readable: sw.Readable(),
		Writable: sw.Writable(),
		IsSmooth: sw.IsSmooth(),
		Unit:     sw.Unit(),
		ValueMin: sw.ValueMin(),
		ValueMax: sw.ValueMax(),
	}
}

func (th *control) Add(sw GenericDevice) {
	th.mtx.Lock()
	defer th.mtx.Unlock()

	th.devMap[sw.UniqueID()] = sw
	sw.Callback(func(thisSw GenericDevice) func() {
		return func() {
			devState := dumpDev(thisSw)

			cmd := jsonCommand{}
			cmd.SerData([]DevState{devState})
			cmd.Cmd = "dev_state"

			timeout := time.After(time.Second * 5)
			select {
			case th.wsOutput <- cmd:
			case <-timeout:
				return
			}
		}
	}(sw))
}

type control struct {
	Binder         func(name string) error
	devMap         map[string]GenericDevice
	cmdChan        chan jsonCommand
	localAPIListen string
	logger         *log.Logger
	cloudDomain    string
	token          string

	wsOutput   chan jsonCommand
	mtx        *sync.Mutex
	stopped    bool
	debug      bool
	tokensaver func(string)

	isConnected bool

	manageWiFi bool
}

func New(LocalAPIAddress string, CloudDomain string, CloudToken string, logger *log.Logger, debug bool, tokensaver func(string), manageWiFi bool) *control {
	th := &control{}
	th.devMap = make(map[string]GenericDevice)
	th.cmdChan = make(chan jsonCommand)
	th.wsOutput = make(chan jsonCommand)
	th.localAPIListen = LocalAPIAddress
	th.logger = logger
	th.cloudDomain = CloudDomain
	th.token = CloudToken
	th.mtx = &sync.Mutex{}
	th.debug = debug
	th.tokensaver = tokensaver
	th.manageWiFi = manageWiFi
	return th
}

type DevState struct {
	ID       string
	Name     string
	State    bool
	Value    float32
	Readable bool
	Writable bool
	IsSmooth bool
	Unit     string
	ValueMin float32
	ValueMax float32
}

type jsonCommand struct {
	Cmd  string
	Data json.RawMessage

	result chan jsonCommand
}

func (th *jsonCommand) SerData(data interface{}) {
	th.Data, _ = json.Marshal(data)
}

func (th *jsonCommand) GetData(obj interface{}) error {
	return json.Unmarshal(th.Data, obj)
}

func (th *control) Stop() {
	close(th.cmdChan)
	th.mtx.Lock()
	defer th.mtx.Unlock()
	th.stopped = true
}

func (th *control) Run() {
	r := gin.New()
	if !th.debug {
		th.logger.Println("GIN Production mode")
		gin.SetMode(gin.ReleaseMode)
	} else {
		th.logger.Println("GIN Debug mode")
		r.Use(gin.Logger())
	}
	r.Use(gin.Recovery())
	r.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
		AllowHeaders:    []string{"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization", "Pragma", "Expires", "Cache-Control", "If-Modified-Since"},
		ExposeHeaders:   []string{"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization", "Pragma", "Expires", "Cache-Control", "If-Modified-Since"},
	}))

	r.POST("/cmd", func(c *gin.Context) {
		cmd := jsonCommand{result: make(chan jsonCommand, 1)}
		if err := c.ShouldBind(&cmd); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		th.cmdChan <- cmd
		timeout := time.After(5 * time.Second)
		select {
		case <-timeout:
			c.JSON(http.StatusInternalServerError, gin.H{"error": "backend timeout"})
			return
		case res := <-cmd.result:
			c.JSON(200, res)
		}
	})

	th.setupRest(r)

	go th.handleJSONCommand()

	server := &http.Server{
		Addr:    th.localAPIListen,
		Handler: r,
	}
	go server.ListenAndServe()

	if th.cloudDomain != "" {
		go th.handleWS()
	}

	if th.manageWiFi {
		go func() {
			st := rpiwifi.GetClientState()
			if st.InetConnected {
				return
			}
			if rpiwifi.APState().ThisAPActive {
				th.logger.Println("AP mode, doing nothing for wifi config")
				return
			}
			disconnected := false
			disconnectedSince := time.Now()
			for {
				time.Sleep(time.Second)

				st := rpiwifi.GetClientState()
				if !disconnected && !st.LANConnected {
					disconnected = true
					disconnectedSince = time.Now()
				} else {
					if st.LANConnected {
						disconnected = false
					}
				}

				if disconnected && time.Since(disconnectedSince) > 2*time.Minute {
					th.logger.Println("Disconnected from LAN, going to AP mode")
					rpiwifi.WriteAPOnlyConfig()
					rpiwifi.Reboot()
					return
				}
			}
		}()
	}
}

func (th *control) handleWS() {
	for {
		th.mtx.Lock()
		stopped := th.stopped
		th.isConnected = false
		token := th.token
		th.mtx.Unlock()
		if stopped {
			return
		}
		time.Sleep(time.Second * 3)

		// th.logger.Printf("token %s", token)
		wsConn, _, err := websocket.DefaultDialer.Dial(fmt.Sprintf("wss://%s/hub/ws?token=%s", th.cloudDomain, token), nil)
		if err != nil {
			th.logger.Println(err.Error())
			continue
		}
		th.mtx.Lock()
		th.isConnected = true
		th.mtx.Unlock()
		th.logger.Printf("connected to %s", th.cloudDomain)
		defer wsConn.Close()
		done := make(chan struct{})

		go func() {
			defer func() {
				close(done)
			}()
			for {
				_, message, err := wsConn.ReadMessage()
				if err != nil {
					return
				}
				cmd := jsonCommand{result: make(chan jsonCommand, 1)}

				if err := json.Unmarshal(message, &cmd); err != nil {
					th.logger.Println(err)
					continue
				}
				th.cmdChan <- cmd
				timeout := time.After(5 * time.Second)
				select {
				case <-timeout:
					continue
				case res := <-cmd.result:
					th.wsOutput <- res
				}
			}
		}()
		go func() {
			for {
				select {
				case <-done:
					return
				case msg := <-th.wsOutput:
					wsConn.WriteJSON(msg)
				}
			}
		}()
		<-done
	}
}
func (th *control) handleJSONCommand() {
	for cmd := range th.cmdChan {
		resp := jsonCommand{}
		resp.Cmd = "error"
		switch cmd.Cmd {
		case "bind":
			dev := struct {
				Name string
			}{}
			if cmd.GetData(&dev) == nil {
				if err := th.Binder(dev.Name); err == nil {
					resp.Cmd = "binding_done"
					resp.SerData(map[string]interface{}{"name": dev.Name})
				} else {
					resp.Cmd = "binding_error"
				}
			}

		case "set_dev_state":
			devStates := []DevState{}
			if cmd.GetData(&devStates) == nil {
				th.mtx.Lock()
				respD := []DevState{}
				for _, devState := range devStates {
					if d, ok := th.devMap[devState.ID]; ok {
						changed := dumpDev(d)
						if !d.IsSmooth() {
							d.CommandState(devState.State)
							changed.State = devState.State
						} else {
							d.CommandValue(devState.Value)
							changed.Value = devState.Value
						}
						respD = append(respD, changed)
					}
				}
				resp.Cmd = "dev_state"
				resp.SerData(respD)
				th.mtx.Unlock()
			}
		case "query_switches":
			th.mtx.Lock()
			devs := []DevState{}
			for _, d := range th.devMap {
				devs = append(devs, dumpDev(d))
			}
			resp.SerData(devs)
			resp.Cmd = "dev_state"
			th.mtx.Unlock()
		case "activate_binding_on_device":
			devState := DevState{}
			if cmd.GetData(&devState) == nil {
				th.mtx.Lock()

				if d, ok := th.devMap[devState.ID]; ok {
					d.ActivateBinding()
					resp.Cmd = "activate_binding_on_device_done"
				}

				th.mtx.Unlock()
			}
		default:
			resp.Cmd = "unknown_command"
		}
		cmd.result <- resp
	}
}

func (th *control) setupRest(r *gin.Engine) {
	if th.manageWiFi {
		r.POST("/wifi", func(c *gin.Context) {
			type WiFiParams struct {
				SSID     string
				Password string
			}
			params := &WiFiParams{}
			if err := c.ShouldBind(&params); err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
				return
			}
			rpiwifi.SaveWiFiCredentials(params.SSID, params.Password)
			rpiwifi.WriteClientOnlyConfig()

			th.logger.Println("Rebooting to apply wificlient conf")
			c.JSON(http.StatusOK, gin.H{"status": "Rebooting to apply wifi client config"})
			rpiwifi.Reboot()
		})

		r.GET("/wifi", func(c *gin.Context) {
			c.JSON(200, rpiwifi.GetClientState())
		})

		r.POST("/reboot", func(c *gin.Context) {
			rpiwifi.Reboot()
			c.Status(200)
		})
	}

	r.GET("/cloud_connection_state", func(c *gin.Context) {
		th.mtx.Lock()
		c.JSON(200, gin.H{"connected": th.isConnected})
		th.mtx.Unlock()
	})

	r.POST("/cloud_connection", func(c *gin.Context) {
		type Account struct {
			Email    string
			Password string
			HubName  string
		}
		params := &Account{}
		if err := c.ShouldBind(&params); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		bytesRepresentation, _ := json.Marshal(map[string]interface{}{"username": params.Email, "password": params.Password})

		resp, err := http.Post(fmt.Sprintf("https://%s/user/auth/login", th.cloudDomain), "application/json", bytes.NewBuffer(bytesRepresentation))
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		type Token struct {
			Token string
		}
		var result Token
		bodyData, _ := ioutil.ReadAll(resp.Body)
		if resp.StatusCode != 200 {
			c.String(resp.StatusCode, string(bodyData))
			return
		}

		if err := json.Unmarshal(bodyData, &result); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		token := result.Token
		if token == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "emtpy access token"})
			return
		}

		client := &http.Client{}
		req, _ := http.NewRequest("GET", fmt.Sprintf("https://%s/user/api/hub_token", th.cloudDomain), nil)
		req.Header.Set("Authorization", "Bearer "+token)
		resp, err = client.Do(req)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		type HubToken struct {
			Hub_token string
		}
		var htr HubToken
		bodyData, _ = ioutil.ReadAll(resp.Body)
		resp.Body.Close()

		if resp.StatusCode != 200 {
			c.String(resp.StatusCode, string(bodyData))
			return
		}

		if err := json.Unmarshal(bodyData, &htr); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		hubToken := htr.Hub_token
		if hubToken == "" {
			c.JSON(http.StatusBadRequest, gin.H{"error": "emtpy hub token"})
			return
		}

		bytesRepresentation, _ = json.Marshal(map[string]interface{}{"token": hubToken, "name": params.HubName})

		req, _ = http.NewRequest("POST", fmt.Sprintf("https://%s/user/api/hubs", th.cloudDomain), bytes.NewBuffer(bytesRepresentation))
		req.Header.Set("Authorization", "Bearer "+token)
		req.Header.Set("Content-Type", "application/json")
		resp, err = client.Do(req)

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		bodyData, _ = ioutil.ReadAll(resp.Body)

		if resp.StatusCode != 201 {
			c.String(resp.StatusCode, string(bodyData))
			return
		}

		th.tokensaver(hubToken)

		th.mtx.Lock()
		th.token = hubToken
		th.mtx.Unlock()

		c.Status(200)
	})

	r.GET("/devices", func(c *gin.Context) {
		cmd := jsonCommand{result: make(chan jsonCommand, 1)}
		cmd.Cmd = "query_switches"
		th.cmdChan <- cmd
		timeout := time.After(5 * time.Second)
		select {
		case <-timeout:
			c.JSON(http.StatusInternalServerError, gin.H{"error": "backend timeout"})
			return
		case res := <-cmd.result:
			c.JSON(200, res)
		}
	})

	r.POST("/devices/state", func(c *gin.Context) {
		cmd := jsonCommand{result: make(chan jsonCommand, 1)}
		cmd.Cmd = "set_dev_state"
		d, _ := c.GetRawData()
		cmd.Data = d
		th.cmdChan <- cmd
		timeout := time.After(5 * time.Second)
		select {
		case <-timeout:
			c.JSON(http.StatusInternalServerError, gin.H{"error": "backend timeout"})
			return
		case res := <-cmd.result:
			c.JSON(200, res)
		}
	})

	r.POST("/bind_device", func(c *gin.Context) {
		cmd := jsonCommand{result: make(chan jsonCommand, 1)}
		cmd.Cmd = "bind"
		d, _ := c.GetRawData()
		cmd.Data = d
		th.cmdChan <- cmd
		timeout := time.After(5 * time.Second)
		select {
		case <-timeout:
			c.JSON(http.StatusInternalServerError, gin.H{"error": "backend timeout"})
			return
		case res := <-cmd.result:
			c.JSON(200, res)
		}
	})

	r.POST("/start_binding_on_device", func(c *gin.Context) {
		cmd := jsonCommand{result: make(chan jsonCommand, 1)}
		cmd.Cmd = "activate_binding_on_device"
		d, _ := c.GetRawData()
		cmd.Data = d
		th.cmdChan <- cmd
		timeout := time.After(5 * time.Second)
		select {
		case <-timeout:
			c.JSON(http.StatusInternalServerError, gin.H{"error": "backend timeout"})
			return
		case res := <-cmd.result:
			c.JSON(200, res)
		}
	})
}
