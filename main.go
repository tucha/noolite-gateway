package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/signal"
	"strconv"

	"sync"
	"syscall"
	"time"

	"log"

	"bitbucket.org/tucha/noolite-gateway/control"
	"bitbucket.org/tucha/noolite-gateway/homekit"
	"github.com/tucher/autosettings"
	"github.com/tucher/gonoolite"
)

type config struct {
	DEBUG   bool
	LogFile string `autosettings:"logfile full path or stdout"`

	NoolitePort string `autosettings:"serialport of Noolite adapter"`
	DeviceFile  string `autosettings:"path to device description file"`

	LocalAPIAddress string `autosettings:"local network api server hostport"`
	CloudToken      string `autosettings:"token to connect to cloud service"`
	CloudDomain     string `autosettings:"cloud service domain"`

	HomekitEnable       bool
	PollIntervalSeconds int64

	ManageWiFi bool
}

func (*config) Default() autosettings.Defaultable {
	return &config{
		DEBUG:               true,
		LogFile:             "stdout",
		NoolitePort:         "/dev/ttyACM0",
		DeviceFile:          "devices.json",
		LocalAPIAddress:     "127.0.0.1:8081",
		HomekitEnable:       false,
		ManageWiFi:          false,
		PollIntervalSeconds: 10,
	}
}

func getLogger(name string, prefix string) *log.Logger {
	var logWriter io.Writer
	if name == "stdout" {
		logWriter = os.Stdout
	} else {
		var err error
		logWriter, err = os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
		if err != nil {
			fmt.Println(err)
			logWriter = os.Stdout
		}
	}
	return log.New(logWriter, prefix+" ", log.Llongfile|log.Ldate|log.Ltime)
}

type br struct {
	id uint32

	name      string
	mtx       sync.Mutex
	state     bool
	callbacks []func()
	backend   *gonoolite.GoNoolite
}

func (th *br) State() bool {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	return th.state
}

func (th *br) Callback(clb func()) {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	th.callbacks = append(th.callbacks, clb)
}

func (th *br) CommandState(st bool) {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	th.backend.SetDeviceState(th.id, st)
}

func (th *br) CommandValue(v float32) {
	th.mtx.Lock()
	defer th.mtx.Unlock()
	if v > 0.5 {
		th.backend.SetDeviceState(th.id, true)
	} else {
		th.backend.SetDeviceState(th.id, false)
	}
}

func (th *br) Description() string { return "Noolite-made relay switch with feedback" }
func (th *br) IsSmooth() bool      { return false }
func (th *br) Readable() bool      { return true }
func (th *br) Writable() bool      { return true }
func (th *br) Unit() string        { return "" }
func (th *br) Value() float32 {
	if th.State() {
		return 1.0
	}
	return 0.0
}
func (th *br) ValueMin() float32 { return 0.0 }
func (th *br) ValueMax() float32 { return 1.0 }

func (th *br) UniqueID() string {
	return strconv.Itoa(int(th.id))
}

func (th *br) Name() string {
	return th.name
}

func (th *br) ActivateBinding() {

}

func main() {

	conf := &config{}
	autosettings.ReadConfig(conf)
	logger := getLogger(conf.LogFile, "main")

	jsonFile, err := os.Open(conf.DeviceFile)
	if err != nil {
		logger.Fatal(err)
	}
	byteValue, _ := ioutil.ReadAll(jsonFile)
	devs := []struct {
		Name string
		ID   uint32
	}{}
	json.Unmarshal(byteValue, &devs)
	jsonFile.Close()

	s, err := gonoolite.New(gonoolite.WithPort(conf.NoolitePort), gonoolite.WithPollInterval(time.Second*time.Duration(conf.PollIntervalSeconds)))
	if err != nil {
		logger.Fatal("Cannot open noolite: ", conf.NoolitePort)
	}

	devChannelMap := map[uint32]*br{}

	homeKit := homekit.New(logger, "12344321", 12345, "./db/")

	control := control.New(conf.LocalAPIAddress, conf.CloudDomain, conf.CloudToken, getLogger("stdout", "control"), true,
		func(t string) {
			conf.CloudToken = t
			bt, _ := json.Marshal(conf)

			var out bytes.Buffer
			json.Indent(&out, bt, "", "  ")

			ioutil.WriteFile("settings.json", out.Bytes(), 0755)
		}, conf.ManageWiFi)
	for _, dd := range devs {
		newSw := &br{name: dd.Name, id: dd.ID, backend: s}
		devChannelMap[dd.ID] = newSw
		homeKit.Add(newSw)
		control.Add(newSw)
	}

	control.Binder = func(name string) error {

		for _, dd := range devs {
			if name == dd.Name {
				return fmt.Errorf("name exists")
			}
		}
		binded := make(chan bool)
		s.OnBinded = func(ch int, devID uint32) {
			logger.Println("Binded to channel ", ch, " dev id ", devID)
			newDev := struct {
				Name string
				ID   uint32
			}{name, devID}
			devs = append(devs, newDev)

			bt, _ := json.Marshal(&devs)
			ioutil.WriteFile(conf.DeviceFile, bt, 0755)

			newSw := &br{name: newDev.Name, id: newDev.ID, backend: s}
			devChannelMap[newDev.ID] = newSw
			homeKit.Stop()
			homeKit.Add(newSw)
			homeKit.Run()
			control.Add(newSw)

			binded <- true
		}
		after := time.After(time.Second * 3)

		s.Bind(0)
		select {
		case <-binded:
			return nil
		case <-after:
			return fmt.Errorf("Nothing was binded! Check if device is close enough and binding mode is enabled")
		}
	}

	if conf.HomekitEnable {
		homeKit.Run()
	}

	control.Run()

	s.OnState = func(devID uint32, st bool) {
		if h, ok := devChannelMap[devID]; ok {
			needNotify := false
			h.mtx.Lock()
			if h.state != st {
				needNotify = true
			}
			h.state = st
			h.mtx.Unlock()

			if needNotify {
				for _, clb := range h.callbacks {
					go clb() //??
				}
			}
		}
	}

	s.SetPolling(true)
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		fmt.Println()
		done <- true
	}()
	<-done
	fmt.Println("exiting")
}
