1) sudo apt-get install dnsmasq hostapd iptables-persistent

/etc/dnsmasq.conf

    interface=lo,uap0
    no-dhcp-interface=lo,wlan0
    bind-interfaces
    server=8.8.8.8
    dhcp-range=10.3.141.50,10.3.141.255,12h


2) /etc/hostapd/hostapd.conf

    interface=uap0
    ssid=_AP_SSID_
    hw_mode=g
    channel=6
    macaddr_acl=0
    auth_algs=1
    ignore_broadcast_ssid=0
    wpa=2
    wpa_passphrase=_AP_PASSWORD_
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=TKIP
    rsn_pairwise=CCMP

3) /etc/default/hostapd
    DAEMON_CONF="/etc/hostapd/hostapd.conf"
sudo systemctl unmask hostapd
sudo systemctl disable hostapd

4)
    echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
    echo 1 > /proc/sys/net/ipv4/ip_forward
    iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
    iptables -A FORWARD -i wlan0 -o uap0 -m state --state RELATED,ESTABLISHED -j ACCEPT
    iptables -A FORWARD -i uap0 -o wlan0 -j ACCEPT
    iptables-save > /etc/iptables/rules.v4

5) /etc/wpa_supplicant/wpa_supplicant.conf

    country=RU
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
    update_config=1

    network={
        ssid="a181"
        psk="PASSWORD"
        id_str="AP1"
        key_mgmt=WPA-PSK
    }
wpa_cli reconfigure

6) /etc/network/interfaces
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto wlan0
allow-hotplug wlan0
iface wlan0 inet manual
    pre-up ifup uap0
    wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
    #pre-down ifdown uap0

iface AP1 inet dhcp

auto uap0
allow-hotplug uap0
iface uap0 inet static
    pre-up iw phy phy0 interface add uap0 type __ap
    post-down iw dev uap0 del
    address 10.3.141.1
    netmask 255.255.255.0
    #hostapd /etc/hostapd/hostapd.conf

7)

To control wlan0 programmatically, modify wpa_supplicant.conf and then 

    (ifdown with --force???)
    (ifdown wlan0)
    ifup wlan0

    
8) 

zeroconf:
sudo apt install avahi-daemon 
sudo micro  /etc/avahi/avahi-daemon.conf
    change "host-name=" line

9) to turn on access point 
    sudo service hostapd start
    sudo service avahi-daemon restart

    to turn it off it is simpler to just restart((