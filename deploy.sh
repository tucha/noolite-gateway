HOST=pi@test-noolite-hub.local
PORT=22
# HOST=pi@noolite.means.live
# PORT=223

GOOS=linux GOARCH=arm GOARM=5  go build
scp -P $PORT ./noolite-gateway $HOST:/home/pi/noolite_hub.update
# scp -P $PORT ./test.py $HOST:/home/pi/test.py

# scp -P $PORT -r ./devices.json  $HOST:/home/pi/
# scp -P $PORT -r ./settings.json $HOST:/home/pi/
ssh -p $PORT -t $HOST 'sudo supervisorctl stop noolite_hub'
ssh -p $PORT -t $HOST 'sudo cp /home/pi/noolite_hub.update /home/pi/noolite_hub'
ssh -p $PORT -t $HOST 'sudo supervisorctl start noolite_hub'
