package rpiwifi

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"strconv"
	"strings"
)

type ClientStatus struct {
	SSID          string
	SignalDBm     int
	APHWAddress   string
	WifiConnected bool
	LANConnected  bool
	InetConnected bool
	IPAddress     string
}
type APStatus struct {
	ThisAPActive bool
}

func runBash(script string) string {
	out, _ := exec.Command("bash", "-c", script).Output()
	txt := strings.TrimSpace(string(out))
	return txt
}
func SaveWiFiCredentials(ssid string, password string) {
	templ := `
	country=RU
	ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
	update_config=1
	
	network={
		ssid="%s"
		psk="%s"
		id_str="AP1"
		key_mgmt=WPA-PSK
	}
	`
	ioutil.WriteFile("/etc/wpa_supplicant/wpa_supplicant.conf", []byte(fmt.Sprintf(templ, ssid, password)), 600)
}
func WriteClientOnlyConfig() {
	templ := `
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto wlan0
allow-hotplug wlan0
iface wlan0 inet manual
	#pre-up ifup uap0
	wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
	#pre-down ifdown uap0

iface AP1 inet dhcp

#auto uap0
#allow-hotplug uap0
#iface uap0 inet static
#	pre-up iw phy phy0 interface add uap0 type __ap
#	post-down iw dev uap0 del
#	address 10.3.141.1
#	netmask 255.255.255.0
#	hostapd /etc/hostapd/hostapd.conf
`
	ioutil.WriteFile("/etc/network/interfaces", []byte(templ), 600)
}

func WriteClientWithAPConfig() {
	templ := `
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto wlan0
allow-hotplug wlan0
iface wlan0 inet manual
	pre-up ifup uap0
	wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
	#pre-down ifdown uap0

iface AP1 inet dhcp

auto uap0
allow-hotplug uap0
iface uap0 inet static
	pre-up iw phy phy0 interface add uap0 type __ap
	post-down iw dev uap0 del
	address 10.3.141.1
	netmask 255.255.255.0
	hostapd /etc/hostapd/hostapd.conf
`
	ioutil.WriteFile("/etc/network/interfaces", []byte(templ), 600)
}
func WriteAPOnlyConfig() {
	templ := `
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

#auto wlan0
#allow-hotplug wlan0
#iface wlan0 inet manual
#	#pre-up ifup uap0
#	wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
#	#pre-down ifdown uap0

#iface AP1 inet dhcp

auto uap0
allow-hotplug uap0
iface uap0 inet static
	pre-up iw phy phy0 interface add uap0 type __ap
	post-down iw dev uap0 del
	address 10.3.141.1
	netmask 255.255.255.0
	hostapd /etc/hostapd/hostapd.conf
`
	ioutil.WriteFile("/etc/network/interfaces", []byte(templ), 600)
}

func GetClientState() (ret ClientStatus) {
	txt := runBash(`iw dev wlan0 link`)
	if strings.Contains(txt, "Not connected") {
		ret.WifiConnected = false
	} else {
		for _, s := range strings.Split(txt, "\n") {
			s = strings.TrimSpace(s)
			if strings.HasPrefix(s, "SSID: ") {
				ret.SSID = s[6:]
			}
			if strings.HasPrefix(s, "Connected to ") && strings.Contains(s, "on wlan0") {
				ret.WifiConnected = true
				ret.APHWAddress = s[13:30]
			}
			if strings.HasPrefix(s, "signal: ") {
				sstr := s[8 : len(s)-4]
				ret.SignalDBm, _ = strconv.Atoi(sstr)
			}
		}
		if ret.WifiConnected {
			ret.IPAddress = runBash(`ip -4 addr show wlan0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'`)
			s := runBash(`ip route | grep default`)
			if strings.HasPrefix(s, "default via ") && strings.HasSuffix(s, "dev wlan0") {
				gateway := s[12 : len(s)-10]
				if strings.Contains(runBash(`ping -c 1 `+gateway), "1 packets transmitted, 1 received, 0% packet loss") {
					ret.LANConnected = true
				}
			}
		}
		if ret.LANConnected {
			if strings.Contains(runBash(`ping -c 1 8.8.8.8`), "1 packets transmitted, 1 received, 0% packet loss") {
				ret.InetConnected = true
			} else {
				ret.InetConnected = false
			}
		}
	}
	return
}
func Reboot() {
	runBash(`reboot`)
}

func StartWifiClient() {
	runBash(`ifup wlan0`)
}

func APState() (ret APStatus) {
	for _, s := range strings.Split(runBash(`ps -ax | grep hostapd`), "\n") {
		if strings.Contains(s, "/usr/sbin/hostapd") && strings.Contains(s, "/etc/hostapd/hostapd.conf") {
			ret.ThisAPActive = true
		}
	}
	return
}
