module bitbucket.org/tucha/noolite-gateway

go 1.14

require (
	github.com/brutella/hc v1.2.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/gorilla/websocket v1.4.2
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/tucher/autosettings v0.0.0-20190609082835-1ae06e020354
	github.com/tucher/gonoolite v0.0.0-20200429101101-ccdf1aaed2ed
	golang.org/x/crypto v0.0.0-20200427165652-729f1e841bcc // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
)
